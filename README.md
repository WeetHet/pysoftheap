# Python library with soft heap

## Installation

The only way to install it is to build the wheel yourself:

```sh
$ matruin build
```

The wheel will be in `target/wheels`, you can install it with pip

## Usage

This library provides only one type: `PySoftHeap`, which takes `eps` as the only constructor argument.

`PySoftHeap` provides three methods:

1. `is_empty() -> bool`, returns if the heap is empty
2. `insert(int)`, inserts a `key` into the heap
3. `pop() -> int | None`, removes minimum by `ckey` from heap, returns it
