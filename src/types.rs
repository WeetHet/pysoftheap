#[derive(Debug, Clone, Copy)]
pub struct ItemCell {
    pub key: i32,
    pub next: usize,
}

#[derive(Debug, Default)]
pub struct HeapNode {
    pub ckey: i32,
    pub rank: usize,
    pub next: usize,
    pub child: usize,
    pub item_list: usize,
    pub item_list_tail: usize,
}

#[derive(Debug, Default)]
pub struct Head {
    pub queue: usize,
    pub next: usize,
    pub prev: usize,
    pub suffix_min: usize,
    pub rank: usize,
}
