use std::ops::{Index, IndexMut};

use crate::types::Head;

pub struct HeadArena {
    heads: Vec<Head>,
}

impl HeadArena {
    pub fn new() -> HeadArena {
        HeadArena {
            heads: vec![Head::default()],
        }
    }

    pub fn new_head(
        &mut self,
        queue: usize,
        next: usize,
        prev: usize,
        suffix_min: usize,
        rank: usize,
    ) -> usize {
        let idx = self.heads.len();
        self.heads.push(Head {
            queue,
            next,
            prev,
            suffix_min,
            rank,
        });
        idx
    }
}

impl Index<usize> for HeadArena {
    type Output = Head;

    fn index(&self, index: usize) -> &Self::Output {
        &self.heads[index]
    }
}

impl IndexMut<usize> for HeadArena {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.heads[index]
    }
}
