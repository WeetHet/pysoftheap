use std::ops::{Index, IndexMut};

use crate::types::HeapNode;

pub struct HeapNodeArena {
    heap_nodes: Vec<HeapNode>,
}

impl HeapNodeArena {
    pub fn new() -> HeapNodeArena {
        HeapNodeArena {
            heap_nodes: vec![HeapNode::default()],
        }
    }

    #[allow(clippy::too_many_arguments)]
    pub fn new_node(
        &mut self,
        ckey: i32,
        rank: usize,
        next: usize,
        child: usize,
        item_list: usize,
        item_list_tail: usize,
    ) -> usize {
        let idx = self.heap_nodes.len();
        self.heap_nodes.push(HeapNode {
            ckey,
            rank,
            next,
            child,
            item_list,
            item_list_tail,
        });
        idx
    }
}

impl Index<usize> for HeapNodeArena {
    type Output = HeapNode;

    fn index(&self, index: usize) -> &Self::Output {
        &self.heap_nodes[index]
    }
}

impl IndexMut<usize> for HeapNodeArena {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.heap_nodes[index]
    }
}
