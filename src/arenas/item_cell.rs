use std::ops::{Index, IndexMut};

use crate::types::ItemCell;

pub struct ItemCellArena {
    item_cells: Vec<ItemCell>,
}

impl ItemCellArena {
    pub fn new() -> ItemCellArena {
        ItemCellArena {
            item_cells: vec![ItemCell { key: 0, next: 0 }],
        }
    }

    pub fn new_item(&mut self, key: i32) -> usize {
        let idx = self.item_cells.len();
        self.item_cells.push(ItemCell { key, next: 0 });
        idx
    }
}

impl Index<usize> for ItemCellArena {
    type Output = ItemCell;

    fn index(&self, index: usize) -> &Self::Output {
        &self.item_cells[index]
    }
}

impl IndexMut<usize> for ItemCellArena {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.item_cells[index]
    }
}
