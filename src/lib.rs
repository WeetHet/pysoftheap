mod arenas;
mod soft_heap;
mod types;

#[cfg(not(test))]
mod python {
    use crate::arenas::{HeadArena, HeapNodeArena, ItemCellArena};
    use crate::soft_heap::SoftHeap;
    use ouroboros::self_referencing;
    use pyo3::prelude::*;

    /// A soft heap implementation in Rust for Python.
    #[pymodule]
    fn softheap(_py: Python, m: &PyModule) -> PyResult<()> {
        m.add_class::<PySoftHeap>()?;
        Ok(())
    }

    #[pyclass]
    #[self_referencing]
    struct PySoftHeap {
        item_cells: ItemCellArena,
        heap_nodes: HeapNodeArena,
        heads: HeadArena,
        #[borrows(mut item_cells, mut heap_nodes, mut heads)]
        #[covariant]
        heap: SoftHeap<'this>,
    }

    #[pymethods]
    impl PySoftHeap {
        #[new]
        /// Create a new soft heap with the given epsilon.
        /// @param eps: The epsilon value for the soft heap.
        fn create(eps: f64) -> Self {
            Self::new(
                ItemCellArena::new(),
                HeapNodeArena::new(),
                HeadArena::new(),
                |item_cells, heap_nodes, heads| SoftHeap::new(eps, item_cells, heap_nodes, heads),
            )
        }

        /// Check if the soft heap is empty.
        /// @return True if the soft heap is empty, False otherwise.
        fn is_empty(&self) -> bool {
            self.borrow_heap().is_empty()
        }

        /// Insert a new key into the soft heap.
        /// @param newkey: The new key to insert.
        fn insert(&mut self, newkey: i32) {
            self.with_heap_mut(|heap| heap.insert(newkey))
        }

        /// Find and remove the minimum key in the soft heap.
        /// @return The minimum key in the soft heap.
        fn pop(&mut self) -> Option<i32> {
            self.with_heap_mut(|heap| heap.pop())
        }
    }
}
