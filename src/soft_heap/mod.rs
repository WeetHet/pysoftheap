use crate::{
    arenas::{HeadArena, HeapNodeArena, ItemCellArena},
    types::Head,
};

pub struct SoftHeap<'a> {
    header: usize,
    tail: usize,
    size: usize,
    r: usize,
    item_cells: &'a mut ItemCellArena,
    nodes: &'a mut HeapNodeArena,
    heads: &'a mut HeadArena,
}

impl<'a> SoftHeap<'a> {
    pub fn new(
        eps: f64,
        item_cell_arena: &'a mut ItemCellArena,
        heap_node_arena: &'a mut HeapNodeArena,
        head_arena: &'a mut HeadArena,
    ) -> SoftHeap<'a> {
        let header = head_arena.new_head(0, 0, 0, 0, 0);
        let tail = head_arena.new_head(0, 0, 0, 0, 0);
        head_arena[header].next = tail;
        head_arena[tail].prev = header;
        head_arena[tail].rank = usize::MAX;

        let r = 2 + 2 * (1.0 / eps).log2().ceil() as usize;
        SoftHeap {
            header,
            tail,
            size: 0,
            r,
            item_cells: item_cell_arena,
            nodes: heap_node_arena,
            heads: head_arena,
        }
    }

    fn meld(&mut self, mut q: usize) {
        let mut tohead = self.heads[self.header].next;
        while self.nodes[q].rank > self.heads[tohead].rank {
            tohead = self.heads[tohead].next;
        }
        let prevhead = self.heads[tohead].prev;
        while self.nodes[q].rank == self.heads[tohead].rank {
            let tohead_ckey = self.nodes[self.heads[tohead].queue].ckey;
            let (top, bottom) = if self.nodes[q].ckey < tohead_ckey {
                (q, self.heads[tohead].queue)
            } else {
                (self.heads[tohead].queue, q)
            };
            q = self.nodes.new_node(
                self.nodes[top].ckey,
                self.nodes[top].rank + 1,
                top,
                bottom,
                self.nodes[top].item_list,
                self.nodes[top].item_list_tail,
            );
            tohead = self.heads[tohead].next;
        }
        let h = if prevhead == self.heads[tohead].prev {
            self.heads.new_head(0, 0, 0, 0, 0)
        } else {
            self.heads[prevhead].next
        };
        self.heads[h] = Head {
            queue: q,
            next: tohead,
            prev: prevhead,
            suffix_min: 0,
            rank: self.nodes[q].rank,
        };
        self.heads[prevhead].next = h;
        self.heads[tohead].prev = h;
        self.fix_minlist(h);
    }

    fn fix_minlist(&mut self, mut h: usize) {
        let mut tmpmin = if self.heads[h].next == self.tail {
            h
        } else {
            self.heads[self.heads[h].next].suffix_min
        };
        while h != self.header {
            if self.nodes[self.heads[h].queue].ckey < self.nodes[self.heads[tmpmin].queue].ckey {
                tmpmin = h;
            }
            self.heads[h].suffix_min = tmpmin;
            h = self.heads[h].prev;
        }
    }

    #[allow(clippy::needless_return)]
    fn sift(&mut self, v: usize) -> usize {
        fn fix_ordering(this: &mut SoftHeap<'_>, v: usize) {
            if this.nodes[this.nodes[v].next].ckey > this.nodes[this.nodes[v].child].ckey {
                let v = &mut this.nodes[v];
                std::mem::swap(&mut v.next, &mut v.child);
            }
        }

        self.nodes[v].item_list = 0;
        self.nodes[v].item_list_tail = 0;
        if self.nodes[v].next == 0 && self.nodes[v].child == 0 {
            self.nodes[v].ckey = i32::MAX;
            return v;
        }
        self.nodes[v].next = self.sift(self.nodes[v].next);
        fix_ordering(self, v);
        self.nodes[v].item_list = self.nodes[self.nodes[v].next].item_list;
        self.nodes[v].item_list_tail = self.nodes[self.nodes[v].next].item_list_tail;
        self.nodes[v].ckey = self.nodes[self.nodes[v].next].ckey;
        if self.nodes[v].rank >= self.r
            && (self.nodes[v].rank % 2 == 1
                || self.nodes[self.nodes[v].child].rank + 1 < self.nodes[v].rank)
        {
            self.nodes[v].next = self.sift(self.nodes[v].next);
            fix_ordering(self, v);
            if self.nodes[self.nodes[v].next].ckey != i32::MAX
                && self.nodes[self.nodes[v].next].item_list != 0
            {
                self.item_cells[self.nodes[self.nodes[v].next].item_list_tail].next =
                    self.nodes[v].item_list;
                self.nodes[v].item_list = self.nodes[self.nodes[v].next].item_list;
                if self.nodes[v].item_list_tail == 0 {
                    self.nodes[v].item_list_tail = self.nodes[self.nodes[v].next].item_list_tail;
                }
                self.nodes[v].ckey = self.nodes[self.nodes[v].next].ckey;
            }
        }
        if self.nodes[self.nodes[v].child].ckey == i32::MAX {
            if self.nodes[self.nodes[v].next].ckey == i32::MAX {
                self.nodes[v].child = 0;
                self.nodes[v].next = 0;
            } else {
                self.nodes[v].child = self.nodes[self.nodes[v].next].child;
                self.nodes[v].next = self.nodes[self.nodes[v].next].next;
            }
        }
        return v;
    }

    pub fn is_empty(&self) -> bool {
        self.size == 0
    }

    pub fn pop(&mut self) -> Option<i32> {
        if self.is_empty() {
            return None;
        }
        let mut h = self.heads[self.heads[self.header].next].suffix_min;
        while self.nodes[self.heads[h].queue].item_list == 0 {
            let (mut tmp, mut childcount) = (self.heads[h].queue, 0);
            while self.nodes[tmp].next != 0 {
                tmp = self.nodes[tmp].next;
                childcount += 1;
            }
            if childcount < self.heads[h].rank / 2 {
                let (h_next, h_prev) = (self.heads[h].next, self.heads[h].prev);
                self.heads[h_prev].next = self.heads[h].next;
                self.heads[h_next].prev = self.heads[h].prev;
                self.fix_minlist(h_prev);
                tmp = self.heads[h].queue;
                while self.nodes[tmp].next != 0 {
                    self.meld(self.nodes[tmp].child);
                    tmp = self.nodes[tmp].next;
                }
            } else {
                self.heads[h].queue = self.sift(self.heads[h].queue);
                if self.nodes[self.heads[h].queue].ckey == i32::MAX {
                    let (h_next, h_prev) = (self.heads[h].next, self.heads[h].prev);
                    self.heads[h_prev].next = self.heads[h].next;
                    self.heads[h_next].prev = self.heads[h].prev;
                }
                self.fix_minlist(h);
            }
            h = self.heads[self.heads[self.header].next].suffix_min;
        }
        let item_list = self.item_cells[self.nodes[self.heads[h].queue].item_list];
        let min = item_list.key;
        self.nodes[self.heads[h].queue].item_list = item_list.next;
        if self.nodes[self.heads[h].queue].item_list == 0 {
            self.nodes[self.heads[h].queue].item_list_tail = 0;
        }

        self.size -= 1;
        Some(min)
    }

    pub fn insert(&mut self, newkey: i32) {
        self.size += 1;
        let icell = self.item_cells.new_item(newkey);
        let q = self.nodes.new_node(newkey, 0, 0, 0, icell, icell);
        self.meld(q);
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use test_strategy::proptest;

    #[proptest]
    fn test_sorted_small_eps(a: Vec<i32>) {
        let mut item_cell_arena = ItemCellArena::new();
        let mut heap_node_arena = HeapNodeArena::new();
        let mut head_arena = HeadArena::new();
        let mut heap = SoftHeap::new(
            1.0 / a.len() as f64,
            &mut item_cell_arena,
            &mut heap_node_arena,
            &mut head_arena,
        );

        for &x in &a {
            heap.insert(x);
        }

        let mut b = a.clone();
        b.sort();
        for &x in &b {
            assert_eq!(Some(x), heap.pop());
        }
    }

    #[proptest]
    fn test_none_disappear(mut a: Vec<i32>) {
        let mut item_cell_arena = ItemCellArena::new();
        let mut heap_node_arena = HeapNodeArena::new();
        let mut head_arena = HeadArena::new();
        let mut heap = SoftHeap::new(
            1.0,
            &mut item_cell_arena,
            &mut heap_node_arena,
            &mut head_arena,
        );

        for &x in &a {
            heap.insert(x);
        }

        a.sort();

        let mut b = vec![];
        while let Some(x) = heap.pop() {
            b.push(x);
        }
        b.sort();
        assert_eq!(a, b);
    }
}
